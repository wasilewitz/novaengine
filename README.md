# NovaEngine #

2D game engine based on SDL2, written in C++ and with support for lua scripting.

### Configuration ###

1. Clone NovaEngine repository.

2. Download `SDL2`, `SDL2_image` and `SDL2_mixer`

3. Set system environment variables:

    `SDL_ROOT` -> SDL2 root catalog
    
    `SDL_IMAGE_ROOT` -> SDL2_image root catalog
    
    `SDL_MIXER_ROOT` -> SDL2_mixer root catalog
    
4. Open in Visual Studio solution of NovaEngine, In properties of project set Configurtaion to "All configurations"

5. In General tab set Configuration Type to "Static library (.lib)"

6. Go to VC++ Directories.

    Paste `$(SDL_ROOT)\include;$(SDL_IMAGE_ROOT)\include;$(SDL_MIXER_ROOT)\include;..\libs\lua_5.1.5\include;..\libs\LuaBridge\include;` in Include Directories
    
    Paste `$(SDL_ROOT)\lib\x86;$(SDL_IMAGE_ROOT)\lib\x86;$(SDL_MIXER_ROOT)\lib\x86;..\libs\lua_5.1.5;` in Library Directories
    
7. Build project

### Game project setup ###
1. Create new Visual Studio C++ project

2. Open project properties.

3. Set Configurations to `All Configurations`

4. In Debugging tab set Working Directory to `$(ProjectDir)..\`

5. VC++ Directories:
    Include Directories: `$(SDL_ROOT)\include;$(SDL_IMAGE_ROOT)\include;$(SDL_MIXER_ROOT)\include;$(NOVA_ROOT)\src;$(NOVA_ROOT)\libs\LuaBridge\include;$(NOVA_ROOT)\libs\lua_5.1.5\include;$(IncludePath)`
    Library Directories: `$(SDL_ROOT)\lib\x86;$(SDL_IMAGE_ROOT)\lib\x86;$(SDL_MIXER_ROOT)\lib\x86;$(NOVA_ROOT)\$(Configuration);$(NOVA_ROOT)\libs\lua_5.1.5\;$(LibraryPath)`

6. Set C/C++ -> Additional -> Include Directories: `$(NOVA_ROOT)`

7. Add `SDL2.lib;SDL2main.lib;SDL2_image.lib;SDL2_mixer.lib;NovaEngine.lib;lua5.1.lib;` to Linker->Input->Additional Dependencies

8. Run project. It will fail because there are missing dll files.

9. Put these dll files to directory with .exe file. `zlib1.dll, SDL2_image.dll, SDL2_mixer.dll, SDL2.dll, libwebp-4.dll, libtiff-5.dll, libpng16-16.dll, libjpeg-9.dll,libfreetype-6.dll`

10. Put spritesheet font with name `debug_overlay_font` to `your_root/res/font` directory. This font will be used to render characters in debug overlay.

11. Add main.cpp with that start code. Black window should appear.
~~~
#include "src\Application.hpp"

int main( int argc, char* args[] )
{
    App.Init( "ExampleGame", 1280, 720 );
    App.MainLoop();
    App.Quit();
    return 0;
}
~~~