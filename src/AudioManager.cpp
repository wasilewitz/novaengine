#include "AudioManager.hpp"

void ReleaseSound( int channel )
{
    auto chunk = Mix_GetChunk( channel );
    NOVA_ASSERT( chunk != nullptr, "Chunk doest exist." );
    Mix_FreeChunk( chunk );
}

void AudioManager::Play( const std::string& soundPath )
{
    Mix_Chunk* sound = Mix_LoadWAV( soundPath.c_str() );
    if( !sound )
    {
        NOVA_ERROR( "Failed to load sound! SDL_mixer error: " + std::string( Mix_GetError() ) );
    }

    Mix_ChannelFinished( ReleaseSound );

    Mix_PlayChannel( -1, sound, 0 );
}
