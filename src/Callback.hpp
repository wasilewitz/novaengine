#ifndef __CALLBACK_HPP__
#define __CALLBACK_HPP__

#include <functional>
#include "CallbackBase.hpp"

template <class T>
class Callback : public CallbackBase
{
public:
    Callback( std::function<void( T )> cb )
        : CallbackBase()
        , m_cb( cb )
    {
    }

    void Execute( T val ) const
    {
        m_cb( val );
    }

private:
    std::function<void( T )> m_cb;
};

#endif // !__CALLBACK_HPP__
