#ifndef __AUDIO_MANGER_HPP__
#define __AUDIO_MANGER_HPP__

#include <SDL_mixer.h>
#include "Utilities.hpp"
class AudioManager
{
public:
    static AudioManager& GetInstance()
    {
        static AudioManager instance;
        return instance;
    }

    void Play( const std::string& soundPath );

private:
    AudioManager()
    {}
};

#endif // !__AUDIO_MANGER_HPP__
