#include "Node.hpp"
#include "Application.hpp"

Node::Node( std::string id )
    : m_id( id )
    , m_nodeType( NodeType::EMPTY_NODE )
    , m_parent( nullptr )
    , m_recalcTransform( true )
    , m_position( 0.0f, 0.0f )
    , m_scale( 1.0f, 1.0f )
    , m_rotationAngle( 0.0f )
    , m_origin( 0.0f, 0.0f )
    , m_isTouch( false )
    , m_luaTouchCb( App.GetLuaState() )
{
}

std::shared_ptr<Node> Node::Create( const std::string& id )
{
    std::shared_ptr<Node> nodePtr( new Node( id ) );
    return nodePtr;
}

Node* Node::l_Create( luabridge::lua_State* L, const std::string& id )
{
    Node* node = new Node( id );
    return node;
}

void Node::AddChild( std::shared_ptr<Node> newChild )
{
    newChild->SetParent( this );
    newChild->RecalcTransform();
    m_children.push_back( newChild );
}

void Node::l_AddChild( Node* newChild )
{
    std::shared_ptr<Node> child( newChild );
    AddChild( child );
}

Node* Node::l_GetChild( const std::string id )
{
    auto child = GetChild<Node>( id );
    return child.get();
}

void Node::RemoveAllChildren()
{
    for( auto& child : m_children )
    {
        child->RemoveAllChildren();
    }
    m_children.clear();
}

void Node::RemoveChild( Node* childToRemove )
{
    m_children.erase( std::remove_if( m_children.begin(), m_children.end(), [=]( std::shared_ptr<Node> child ){
        return child.get() == childToRemove;
    } ), m_children.end() );
}

void Node::Remove()
{
    RemoveAllChildren();
    m_parent->RemoveChild( this );
}

void Node::Draw()
{
    DrawChildren();
}

void Node::Update( uint32_t dt )
{
    Invalidate();
    for( auto child : m_children )
    {
        child->Update( dt );
    }
}

int Node::l_GetPosition( luabridge::lua_State* L )
{
    luabridge::push( L, m_position.GetX() );
    luabridge::push( L, m_position.GetY() );
    return 2;
}

int Node::l_GetOrigin( luabridge::lua_State* L )
{
    luabridge::push( L, m_origin.GetX() );
    luabridge::push( L, m_origin.GetY() );
    return 2;
}

int Node::l_GetScale( luabridge::lua_State * L )
{
    luabridge::push( L, m_scale.GetX() );
    luabridge::push( L, m_scale.GetY() );
    return 2;
}

void Node::Move( Vector2f movementValue )
{
    auto& currentPosition = GetPosition();
    SetPosition( currentPosition + movementValue );
}

void Node::Move( float movementValueX, float movementValueY )
{
    Move( Vector2f( movementValueX, movementValueY ) );
}

void Node::DisableTouch()
{
    m_isTouch = false;
    m_touchCb = []( float posX, float posY ) {
    };
}

void Node::l_DisableTouch()
{
    m_isTouch = false;
    m_luaTouchCb = luabridge::LuaRef( App.GetLuaState() );
}

bool Node::HandleTouch( const Vector2f& worldTouchPos )
{
    if( m_isTouch )
    {
        auto& nodeGlobalPos = GetGlobalPosition();
        auto& nodeSize = GetSize();
        if( worldTouchPos.GetX() >= nodeGlobalPos.GetX()
            && worldTouchPos.GetX() <= nodeGlobalPos.GetX() + nodeSize.GetX()
            && worldTouchPos.GetY() >= nodeGlobalPos.GetY()
            && worldTouchPos.GetY() <= nodeGlobalPos.GetY() + nodeSize.GetY() )
        {
            bool touchHandled = false;
            if( m_touchCb )
            {
                m_touchCb( worldTouchPos.GetX() - nodeGlobalPos.GetX(), worldTouchPos.GetY() - nodeGlobalPos.GetY() );
                touchHandled = true;
            }
            if( !m_luaTouchCb.isNil() )
            {
                m_luaTouchCb( worldTouchPos.GetX() - nodeGlobalPos.GetX(), worldTouchPos.GetY() - nodeGlobalPos.GetY() );
                touchHandled = true;
            }
            if( touchHandled )
            {
                return true;
            }
        }
    }
    else
    {
        for( int i = 0; i < m_children.size(); ++i )
        {
            bool handled = m_children[i]->HandleTouch( worldTouchPos );
            if( handled )
            {
                break;
            }
        }
    }
    return false;
}

void Node::Invalidate()
{
    if( m_recalcTransform )
    {
        float angle = -m_rotationAngle * PI / 180.f;
        float cos = cosf( angle );
        float sin = sinf( angle );
        float sxc = m_scale.GetX() * cos;
        float syc = m_scale.GetY() * cos;
        float sxs = m_scale.GetX() * sin;
        float sys = m_scale.GetY() * sin;
        Vector2f parentOrigin = m_parent ? m_parent->GetOrigin() : Vector2f( 0, 0 );
        float tx = -m_origin.GetX() * sxc - m_origin.GetY() * sys + m_position.GetX();
        float ty = m_origin.GetX() * sxs - m_origin.GetY() * syc + m_position.GetY();

        m_localMatrix[0] = sxc;
        m_localMatrix[1] = -sxs;
        m_localMatrix[2] = sys;
        m_localMatrix[3] = syc;
        m_localMatrix[4] = tx;
        m_localMatrix[5] = ty;

        m_globalMatrix = m_parent ? m_parent->GetGlobalMatrix() * m_localMatrix : m_localMatrix;

        for( auto& child : m_children )
        {
            child->RecalcTransform();
        }

        m_recalcTransform = false;
    }
}

void Node::DrawChildren()
{
    for( auto& child : m_children )
    {
        child->Draw();
    }
}
