#ifndef __CALLBACK_LUA_HPP__
#define __CALLBACK_LUA_HPP__

#include <LuaBridge.h>
#include "CallbackBase.hpp"

template <class T>
class CallbackLua : public CallbackBase
{
public:
    CallbackLua( luabridge::LuaRef cb )
        : CallbackBase()
        , m_cb( cb )
    {
    }

    void Execute( T val ) const
    {
        m_cb( val );
    }

private:
    luabridge::LuaRef m_cb;
};

#endif // !__CALLBACK_LUA_HPP__
