#ifndef __TEXTURE_MANAGER_HPP__
#define __TEXTURE_MANAGER_HPP__

#include "TextureResource.hpp"
#include <unordered_map>

class TextureManager
{
public:
    static TextureManager& GetInstance()
    {
        static TextureManager instance;
        return instance;
    }

    TextureData* GetTexture( const std::string& path );
    void ReleaseTexture( const std::string& path );

private:
    TextureManager() {}

    std::unordered_map<std::string, std::shared_ptr<TextureResource>> m_textureResourceMap;
};

#endif // !__TEXTURE_MANAGER_HPP__
