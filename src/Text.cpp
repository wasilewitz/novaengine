#include "Text.hpp"
#include "FontsManager.hpp"

std::shared_ptr<Text> Text::Create( const std::string &id, const std::string& path, const std::string& text )
{
    std::shared_ptr<Text> textPtr( new Text( id, path, text ) );
    return textPtr;
}

Text* Text::l_Create( luabridge::lua_State* L, std::string id, std::string path, std::string text )
{
    Text* textPtr = new Text( id, path, text );
    return textPtr;
}

void Text::SetText( const std::string& text )
{
    m_text = text;
    SetupCharactersSprites();
}

int Text::l_GetSize( luabridge::lua_State* L )
{
    luabridge::push( L, m_size.GetX() );
    luabridge::push( L, m_size.GetY() );
    return 2;
}

Text::Text( const std::string& id, const std::string& path, const std::string& text )
    : Node( id )
    , m_text( text )
    , m_fontPath( path )
    , m_charactersGap( 5 )
{
    m_nodeType = NodeType::TEXT;
    SetupCharactersSprites();
}

void Text::SetupCharactersSprites()
{
    this->RemoveAllChildren();

    auto font = FontsManager::GetInstance().GetFont( m_fontPath );
    float width = 0;
    float height = 0;
    for( int i = 0; i < m_text.size(); ++i )
    {
        auto characterRect = font->GetCharacterRect( m_text[i] );
        auto characterSprite = Sprite::Create( "", font->GetSourceImagePath(), characterRect );
        characterSprite->SetPosition( width, 0 );
        AddChild( characterSprite );
        width += m_charactersGap + characterRect.GetSize().GetX();
        if( characterRect.GetSize().GetY() > height )
        {
            height = characterRect.GetSize().GetY();
        }
    }
    m_size.Set( width, height );
}
