#ifndef __SPRITE_ANIMATION_HPP__
#define __SPRITE_ANIMATION_HPP__

#include "Sprite.hpp"
#include <map>

class AnimationInfo;

class SpriteAnimation : public Sprite
{
public:
    static std::shared_ptr<SpriteAnimation> LoadFromXml( const std::string& xmlFile );
    static SpriteAnimation* l_LoadFromXML( luabridge::lua_State* L, const std::string xmlFile );

    void Play( bool looped = false );
    void Stop() { m_play = false; }
    void Update( uint32_t dt );
    void SetAnimation( std::string name );
    void AddAnimation( std::string name, uint32_t startFrame, uint32_t endFrame );
    std::string GetAnimation() { return m_currentAnimationName; }
    const Vector2f GetSize() const { return m_frameSize; }
    int l_GetSize( luabridge::lua_State* L );
    bool IsFinished() const { return m_finished; }

private:
    SpriteAnimation( const std::string& id, const std::string& path, uint16_t amountOfKeyframes, Vector2i frameSize );
    static SpriteAnimation* LocalCreate( const std::string& xmlFile );
    void NextFrame();

    uint16_t m_amountOfKeyframes;
    Vector2f m_frameSize;
    uint16_t m_fps;
    std::vector<Rect> m_keyframes;
    uint16_t m_currentFrame;
    uint64_t m_lastTimestamp;
    bool m_play;
    bool m_looped;
    bool m_finished;

    std::map<std::string, AnimationInfo> m_animations;
    std::string m_currentAnimationName;
};

class AnimationInfo
{
public:
    AnimationInfo( uint32_t startFrame, uint32_t endFrame )
        : m_startFrame( startFrame )
        , m_endFrame( endFrame )
    {
    }

    uint32_t GetStartFrame() { return m_startFrame; }
    uint32_t GetEndFrame() { return m_endFrame; }

private:
    uint32_t m_startFrame, m_endFrame;
};

#endif // !__SPRITE_ANIMATION_HPP__
