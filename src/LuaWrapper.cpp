#include "LuaWrapper.hpp"

void LuaWrapper::DoFile( const std::string& fileName )
{
    if( luaL_dofile( App.GetLuaState(), fileName.c_str() ) )
    {
        NOVA_ERROR( lua_tostring( App.GetLuaState(), -1 ) );
    }
}
