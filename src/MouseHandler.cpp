#include "MouseHandler.hpp"

MouseHandler::MouseHandler()
    : m_mouseButtonsMap( luabridge::newTable( App.GetLuaState() ) )
{
    /* Create map of mouse buttons as a lua table. */
    MapMouseButtonsToLuaTable();

    App.RegisterUpdateCallback( "MouseHandlerUpdate", [&]( uint32_t dt ) {
        auto mousePos = GetMappedMousePointerPos();
        for( auto& listener : m_mouseButtonsBeingPressedListeners )
        {
            listener.second( dt, m_mouseButtonsState, mousePos.GetX(), mousePos.GetY() );
        }

        luabridge::LuaRef mouseButtonsBeingPressedMap = luabridge::newTable( App.GetLuaState() );
        for( luabridge::Iterator iter( m_mouseButtonsMap ); !iter.isNil(); ++iter )
        {
            luabridge::LuaRef value = *iter;
            if( m_mouseButtonsState[value.cast<int>()] )
            {
                mouseButtonsBeingPressedMap[iter.key()] = true;
            }
        }

        for( auto& luaListener : m_luaMouseButtonsBeingPressedListeners )
        {
            luaListener.second( dt, mouseButtonsBeingPressedMap, mousePos.GetX(), mousePos.GetY() );
        }
    } );
}

void MouseHandler::MapMouseButtonsToLuaTable()
{
    m_mouseButtonsMap["LEFT"] = (int)MouseButton::LEFT;
    m_mouseButtonsMap["MIDDLE"] = (int)MouseButton::MIDDLE;
    m_mouseButtonsMap["RIGHT"] = (int)MouseButton::RIGHT;
}

void MouseHandler::HandleEvent( SDL_Event& e )
{
    auto mousePos = GetMappedMousePointerPos();

    if( e.type == SDL_MOUSEMOTION )
    {
        for( auto& listener : m_mouseMovedListeners )
        {
            listener.second( mousePos.GetX(), mousePos.GetY() );
        }
        for( auto& luaListener : m_luaMouseMovedListeners )
        {
            luaListener.second( mousePos.GetX(), mousePos.GetY() );
        }
    }
    else if( e.type == SDL_MOUSEBUTTONDOWN )
    {
        MouseButton buttonClicked;
        switch( e.button.button )
        {
            case SDL_BUTTON_LEFT:
                buttonClicked = LEFT;
                break;
            case SDL_BUTTON_RIGHT:
                buttonClicked = RIGHT;
                break;
            case SDL_BUTTON_MIDDLE:
                buttonClicked = MIDDLE;
                break;
            default:
                NOVA_ASSERT( false, std::string( "Unknown mouse button clicked!" ) );
                break;
        }
        m_mouseButtonsState[buttonClicked] = true;

        for( auto& listener : m_mouseButtonDownListeners )
        {
            listener.second( buttonClicked, mousePos.GetX(), mousePos.GetY() );
        }
        for( auto& luaListener : m_luaMouseButtonDownListeners )
        {
            luaListener.second( (int)buttonClicked, mousePos.GetX(), mousePos.GetY() );
        }

        App.HandleTouch( mousePos );
    }
    else if( e.type == SDL_MOUSEBUTTONUP )
    {
        MouseButton buttonReleased;
        switch( e.button.button )
        {
            case SDL_BUTTON_LEFT:
                buttonReleased = LEFT;
                break;
            case SDL_BUTTON_RIGHT:
                buttonReleased = RIGHT;
                break;
            case SDL_BUTTON_MIDDLE:
                buttonReleased = MIDDLE;
                break;
            default:
                NOVA_ASSERT( false, std::string( "Unknown mouse button released!" ) );
                break;
        }
        m_mouseButtonsState[buttonReleased] = false;

        for( auto& listener : m_mouseButtonUpListeners )
        {
            listener.second( buttonReleased, mousePos.GetX(), mousePos.GetY() );
        }
        for( auto& luaListener : m_luaMouseButtonUpListeners )
        {
            luaListener.second( (int)buttonReleased, mousePos.GetX(), mousePos.GetY() );
        }
    }
}

void MouseHandler::RegisterOnMouseMovedListener( std::string id, std::function<void( int posX, int posY )> func )
{
    auto listener = m_mouseMovedListeners.find( id );
    NOVA_ASSERT( listener == m_mouseMovedListeners.end(), "Listener with id: " + id + " already exists!" );
    m_mouseMovedListeners.emplace( id, func );
}

void MouseHandler::l_RegisterOnMouseMovedListener( const std::string listenerId, luabridge::LuaRef func )
{
    auto listener = m_luaMouseMovedListeners.find( listenerId );
    NOVA_ASSERT( listener == m_luaMouseMovedListeners.end(), "Listener with id: " + listenerId + " already exists!" );
    m_luaMouseMovedListeners.emplace( listenerId, func );
}

void MouseHandler::UnregisterOnMouseMovedListener( const std::string listenerId )
{
    auto listener = m_mouseMovedListeners.find( listenerId );
    NOVA_ASSERT( listener != m_mouseMovedListeners.end(), "Listener with id: " + listenerId + " doesn't exist!" );
    m_mouseMovedListeners.erase( listener );
}

void MouseHandler::l_UnregisterOnMouseMovedListener( const std::string listenerId )
{
    auto listener = m_luaMouseMovedListeners.find( listenerId );
    NOVA_ASSERT( listener != m_luaMouseMovedListeners.end(), "Listener with id: " + listenerId + " doesn't exist!" );
    m_luaMouseMovedListeners.erase( listener );
}

void MouseHandler::UnregisterOnMouseMovedListeners()
{
    m_mouseMovedListeners.clear();
}

void MouseHandler::RegisterOnMouseButtonUpListener( std::string id, std::function<void( MouseButton button, int posX, int posY )> func )
{
    auto listener = m_mouseButtonUpListeners.find( id );
    NOVA_ASSERT( listener == m_mouseButtonUpListeners.end(), "Listener with id: " + id + " already exists!" );
    m_mouseButtonUpListeners.emplace( id, func );
}

void MouseHandler::l_RegisterOnMouseButtonUpListener( const std::string listenerId, luabridge::LuaRef func )
{
    auto listener = m_luaMouseButtonUpListeners.find( listenerId );
    NOVA_ASSERT( listener == m_luaMouseButtonUpListeners.end(), "Listener with id: " + listenerId + " already exists!" );
    m_luaMouseButtonUpListeners.emplace( listenerId, func );
}

void MouseHandler::UnregisterOnMouseButtonUpListener( const std::string listenerId )
{
    auto listener = m_mouseButtonUpListeners.find( listenerId );
    NOVA_ASSERT( listener != m_mouseButtonUpListeners.end(), "Listener with id: " + listenerId + " doesn't exist!" );
    m_mouseButtonUpListeners.erase( listener );
}

void MouseHandler::l_UnregisterOnMouseButtonUpListener( const std::string listenerId )
{
    auto listener = m_luaMouseButtonUpListeners.find( listenerId );
    NOVA_ASSERT( listener != m_luaMouseButtonUpListeners.end(), "Listener with id: " + listenerId + " doesn't exist!" );
    m_luaMouseButtonUpListeners.erase( listener );
}

void MouseHandler::UnregisterOnMouseButtonUpListeners()
{
    m_mouseButtonUpListeners.clear();
    m_luaMouseButtonUpListeners.clear();
}

void MouseHandler::RegisterOnMouseButtonDownListener( std::string id, std::function<void( MouseButton button, int posX, int posY )> func )
{
    auto listener = m_mouseButtonDownListeners.find( id );
    NOVA_ASSERT( listener == m_mouseButtonDownListeners.end(), "Listener with id: " + id + " already exists!" );
    m_mouseButtonDownListeners.emplace( id, func );
}

void MouseHandler::l_RegisterOnMouseButtonDownListener( const std::string listenerId, luabridge::LuaRef func )
{
    auto listener = m_luaMouseButtonDownListeners.find( listenerId );
    NOVA_ASSERT( listener == m_luaMouseButtonDownListeners.end(), "Listener with id: " + listenerId + " already exists!" );
    m_luaMouseButtonDownListeners.emplace( listenerId, func );
}

void MouseHandler::UnregisterOnMouseButtonDownListener( const std::string listenerId )
{
    auto listener = m_mouseButtonDownListeners.find( listenerId );
    NOVA_ASSERT( listener != m_mouseButtonDownListeners.end(), "Listener with id: " + listenerId + " doesn't exist!" );
    m_mouseButtonDownListeners.erase( listener );
}

void MouseHandler::l_UnregisterOnMouseButtonDownListener( const std::string listenerId )
{
    auto listener = m_luaMouseButtonDownListeners.find( listenerId );
    NOVA_ASSERT( listener != m_luaMouseButtonDownListeners.end(), "Listener with id: " + listenerId + " doesn't exist!" );
    m_luaMouseButtonDownListeners.erase( listener );
}

void MouseHandler::UnregisterOnMouseButtonDownListeners()
{
    m_mouseButtonDownListeners.clear();
    m_luaMouseButtonDownListeners.clear();
}

void MouseHandler::RegisterOnMouseButtonsBeingPressedListener( std::string id, std::function<void( uint32_t dt, bool* mouseButtonsState, int posX, int posY )> func )
{
    auto listener = m_mouseButtonsBeingPressedListeners.find( id );
    NOVA_ASSERT( listener == m_mouseButtonsBeingPressedListeners.end(), "Listener with id: " + id + " already exists!" );
    m_mouseButtonsBeingPressedListeners.emplace( id, func );
}

void MouseHandler::l_RegisterOnMouseButtonsBeingPressedListener( const std::string listenerId, luabridge::LuaRef func )
{
    auto listener = m_luaMouseButtonsBeingPressedListeners.find( listenerId );
    NOVA_ASSERT( listener == m_luaMouseButtonsBeingPressedListeners.end(), "Listener with id: " + listenerId + " already exists!" );
    m_luaMouseButtonsBeingPressedListeners.emplace( listenerId, func );
}

void MouseHandler::UnregisterOnMouseButtonsBeingPressedListener( const std::string listenerId )
{
    auto listener = m_mouseButtonsBeingPressedListeners.find( listenerId );
    NOVA_ASSERT( listener != m_mouseButtonsBeingPressedListeners.end(), "Listener with id: " + listenerId + " doesn't exist!" );
    m_mouseButtonsBeingPressedListeners.erase( listener );
}

void MouseHandler::l_UnregisterOnMouseButtonsBeingPressedListener( const std::string listenerId )
{
    auto listener = m_luaMouseButtonsBeingPressedListeners.find( listenerId );
    NOVA_ASSERT( listener != m_luaMouseButtonsBeingPressedListeners.end(), "Listener with id: " + listenerId + " doesn't exist!" );
    m_luaMouseButtonsBeingPressedListeners.erase( listener );
}

void MouseHandler::UnregisterOnMouseButtonsBeingPressedListeners()
{
    m_mouseButtonsBeingPressedListeners.clear();
    m_luaMouseButtonsBeingPressedListeners.clear();
}

Vector2i MouseHandler::GetMappedMousePointerPos()
{
    int x, y;
    SDL_GetMouseState( &x, &y );
    float screenResToWindowSizeRatio = App.GetScreenResToWindowSizeRatio();
    x *= screenResToWindowSizeRatio;
    y *= screenResToWindowSizeRatio;

    return Vector2i( x, y );
}

void MouseHandler::Release()
{
    UnregisterOnMouseMovedListeners();
    UnregisterOnMouseButtonUpListeners();
    UnregisterOnMouseButtonDownListeners();
    UnregisterOnMouseButtonsBeingPressedListeners();
    App.UnregisterUpdateCallback( "MouseHandlerUpdate" );
}
