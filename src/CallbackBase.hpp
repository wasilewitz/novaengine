#ifndef __CALLBACK_BASE_HPP__
#define __CALLBACK_BASE_HPP__

class CallbackBase
{
public:
    CallbackBase()
        : m_outOfDate( false )
    {
    }

    void SetOutOfDate()
    {
        m_outOfDate = true;
    }

    bool IsOutOfDate() const
    {
        return m_outOfDate;
    }

protected:
    bool m_outOfDate;
};

#endif // !__CALLBACK_BASE_HPP__
