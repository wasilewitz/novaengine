#ifndef __FONTS_MANAGER_HPP__
#define __FONTS_MANAGER_HPP__
#include <map>
#include <string>
#include <memory>
#include "Rect.hpp"

struct FontCharacter
{
    FontCharacter( int asciiCode, Rect rect )
        : m_asciiCode( asciiCode )
        , m_rect( rect )
    {
    }

    int m_asciiCode;
    Rect m_rect;
};

class Font
{
public:
    Font( const std::string& path );
    const std::string& GetSourceImagePath() { return m_sourceImagePath; }
    Rect& GetCharacterRect( int asciiCode );

private:
    std::map<int, std::shared_ptr<FontCharacter>> m_characters;
    std::string m_sourceImagePath;
};

class FontsManager
{
public:
    static FontsManager& GetInstance()
    {
        static FontsManager instance;
        return instance;
    }

    std::shared_ptr<Font> GetFont( const std::string& path );

private:
    std::map<std::string, std::shared_ptr<Font>> m_fonts;
};

#endif // !__FONTS_MANAGER_HPP__
