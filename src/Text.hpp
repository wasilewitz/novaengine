#ifndef __TEXT_HPP__
#define __TEXT_HPP__

#include "Sprite.hpp"

class Text : public Node
{
public:
    static std::shared_ptr<Text> Create( const std::string& id, const std::string& path, const std::string& text );
    static Text* l_Create( luabridge::lua_State* L, std::string id, std::string path, std::string text );

    void SetText( const std::string& text );
    const Vector2f GetSize() const { return m_size; }
    int l_GetSize( luabridge::lua_State* L );

private:
    Text( const std::string& id, const std::string& path, const std::string& text );
    void SetupCharactersSprites();

    std::string m_text;
    std::string m_fontPath;
    int m_charactersGap;
    Vector2f m_size;
};

#endif // !__TEXT_HPP__