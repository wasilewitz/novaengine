#ifndef __VIEWPORT_HPP__
#define __VIEWPORT_HPP__
#include "Math\Vector2.hpp"
#include "Node.hpp"

class Viewport
{
public:
    static Viewport& GetInstance()
    {
        static Viewport instance;
        return instance;
    }

    void SetPosition( Vector2f position );
    void SetPosition( float x, float y );
    void l_SetPosition( float x, float y );
    const Vector2f GetPosition() const { return m_position; }
    int l_GetPosition( luabridge::lua_State* L );

private:
    Vector2f m_position;
};

#endif // !__VIEWPORT_HPP__
