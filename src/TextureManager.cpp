#include "TextureManager.hpp"

TextureData* TextureManager::GetTexture( const std::string& path )
{
    auto& element = m_textureResourceMap.find( path );
    if( element == m_textureResourceMap.end() )
    {
        auto& newElement = std::make_shared<TextureResource>( path );
        m_textureResourceMap.insert( std::make_pair( path, newElement ) );
        return newElement->GetTextureData();
    }
    else
    {
        auto& textureResource = element->second;
        textureResource->AddReference();
        return textureResource->GetTextureData();
    }
}

void TextureManager::ReleaseTexture( const std::string& path )
{
    auto& element = m_textureResourceMap.find( path );
    NOVA_ASSERT( element != m_textureResourceMap.end(), "There is no texture " + path + " in texture manager." );
    auto& textureResource = element->second;
    textureResource->RemoveReference();
    if( textureResource->GetReferenceCount() == 0 )
    {
        m_textureResourceMap.erase( path );
    }
}
