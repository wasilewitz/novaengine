#ifndef __NODE_HPP__
#define __NODE_HPP__

#include <string>
#include <vector>
#include <memory>
#include "Math\Math.hpp"
#include "Utilities.hpp"
#include <LuaBridge.h>
#include <functional>

using namespace Math;

enum NodeType { EMPTY_NODE, SPRITE, SPRITE_ANIMATION, TEXT };

class Node
{
public:
    Node( std::string id );
    virtual ~Node() {}

    static std::shared_ptr<Node> Create( const std::string& id );
    static Node* l_Create( luabridge::lua_State* L, const std::string& id );

    void AddChild( std::shared_ptr<Node> newChild );
    void l_AddChild( Node* newChild );
    template<class T>
    std::shared_ptr<T> GetChild( std::string id );
    Node* l_GetChild( const std::string id );
    void SetParent( Node* parent ) { m_parent = parent; }
    void RecalcTransform() { m_recalcTransform = true; }
    void RemoveAllChildren();
    void RemoveChild( Node* childToRemove );
    void Remove();
    virtual void Draw();

    virtual void Update( uint32_t dt );

    Matrix3x2f GetGlobalMatrix() { return m_globalMatrix; }
    Matrix3x2f GetLocalMatrix() { return m_localMatrix; }

    std::string GetID() { return m_id; }

    Vector2f GetPosition() { return m_position; }
    int l_GetPosition( luabridge::lua_State* L );
    Vector2f GetOrigin() { return m_origin; }
    int l_GetOrigin( luabridge::lua_State* L );
    Vector2f GetScale() { return m_scale; }
    int l_GetScale( luabridge::lua_State* L );
    float GetRotation() { return m_rotationAngle; }
    virtual const Vector2f GetSize() const { return Vector2f( 0.0f, 0.0f ); }

    void SetPosition( float x, float y ) { m_position = Vector2f( x, y ); RecalcTransform(); }
    void SetPosition( Vector2f position ) {  m_position = position; RecalcTransform(); }
    void l_SetPosition( float x, float y ) { SetPosition( x, y ); }
    void Move( Vector2f movementValue );
    void Move( float movementValueX, float movementValueY );
    void l_Move( float x, float y ) { Move( x, y ); }
    void SetScale( float x, float y ) { m_scale = Vector2f( x, y ); RecalcTransform(); }
    void SetScale( Vector2f scale ) { m_scale = scale; RecalcTransform(); }
    void SetScale( float scale ) { SetScale( scale, scale ); RecalcTransform(); }
    void l_SetScale( float x, float y ) { SetScale( x, y ); }
    void SetRotation( float angle ) { m_rotationAngle = angle; RecalcTransform(); }
    void SetOrigin( Vector2f origin ) { m_origin = origin; RecalcTransform(); }
    void SetOrigin( float x, float y ) { m_origin = Vector2f( x, y ); RecalcTransform(); }
    void l_SetOrigin( float x, float y ) { SetOrigin( x, y ); }
    void EnableTouch( std::function<void( float, float )> cb ) { m_isTouch = true; m_touchCb = cb; } //Add callback
    void l_EnableTouch( luabridge::LuaRef func ) { m_isTouch = true, m_luaTouchCb = func; }
    void DisableTouch();
    void l_DisableTouch();
    bool HandleTouch( const Vector2f& worldTouchPos );

protected:
    void Invalidate();
    void DrawChildren();

    const Vector2f GetGlobalPosition() { return Vector2f( m_globalMatrix[4], m_globalMatrix[5] ); }
    Vector2f GetGlobalScale() { return Vector2f( hypotf( m_globalMatrix[0], m_globalMatrix[1] ), hypotf( m_globalMatrix[2], m_globalMatrix[3] ) ); }
    float GetGlobalRotation() { return atan2f( m_globalMatrix[1], m_globalMatrix[0] ) * 180.0f / PI; }

    NodeType m_nodeType;
    std::vector<std::shared_ptr<Node>> m_children;
    Node* m_parent;
    Matrix3x2f m_globalMatrix;
    Matrix3x2f m_localMatrix;

    bool m_recalcTransform;

    std::string m_id;
    Vector2f m_position;
    Vector2f m_origin;
    Vector2f m_scale;
    float m_rotationAngle;

    bool m_isTouch;
    luabridge::LuaRef m_luaTouchCb;
    std::function<void( float posX, float posY )> m_touchCb;
};

template<class T>
inline std::shared_ptr<T> Node::GetChild( std::string id )
{
    for( auto& child : m_children )
    {
        if( child->GetID() == id )
        {
            return std::dynamic_pointer_cast<T>( child );
        }
    }
    NOVA_ASSERT( false, "Child with id: " + id + " doesn't exist." );
    return nullptr;
}

#endif // !__NODE_HPP__