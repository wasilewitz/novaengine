#include "SpriteAnimation.hpp"
#include "Application.hpp"
#include "tinyxml2.h"

SpriteAnimation* SpriteAnimation::LocalCreate( const std::string& xmlFile )
{
    tinyxml2::XMLDocument animationFile;
    animationFile.LoadFile( xmlFile.c_str() );
    auto root = animationFile.RootElement();

    std::string src = root->FirstChildElement( "src" )->GetText();
    std::string frameWidth = root->FirstChildElement( "frameWidth" )->GetText();
    std::string frameHeight = root->FirstChildElement( "frameHeight" )->GetText();
    std::string amountOfKeyFrames = root->FirstChildElement( "amountOfKeyFrames" )->GetText();
    std::string id = root->FirstChildElement( "id" )->GetText();

    SpriteAnimation* spriteAnim = new SpriteAnimation( id, src, std::stoi( amountOfKeyFrames ), Vector2i( std::stoi( frameWidth ), std::stoi( frameHeight ) ) );

    for( auto anim = root->FirstChildElement( "animation" ); anim; anim = anim->NextSiblingElement( "animation" ) )
    {
        std::string name = anim->Attribute( "name" );
        std::string startFrame = anim->Attribute( "startFrame" );
        std::string endFrame = anim->Attribute( "endFrame" );

        spriteAnim->AddAnimation( name, std::stoi( startFrame ), std::stoi( endFrame ) );
    }

    return spriteAnim;
}

std::shared_ptr<SpriteAnimation> SpriteAnimation::LoadFromXml( const std::string& xmlFile )
{
    auto spriteAnim = LocalCreate( xmlFile );
    return std::shared_ptr<SpriteAnimation>( spriteAnim );
}

SpriteAnimation* SpriteAnimation::l_LoadFromXML( luabridge::lua_State* L, const std::string xmlFile )
{
    auto spriteAnim = LocalCreate( xmlFile );
    return spriteAnim;
}

SpriteAnimation::SpriteAnimation( const std::string& id, const std::string& path, uint16_t amountOfKeyframes, Vector2i frameSize )
    : Sprite( id, path, Rect( 0, 0, frameSize.GetX(), frameSize.GetY() ) )
    , m_amountOfKeyframes( amountOfKeyframes )
    , m_frameSize( frameSize )
    , m_fps( 24 )
    , m_currentFrame( 0 )
    , m_lastTimestamp( 0 )
    , m_play( false )
    , m_looped( false )
    , m_finished( false )
{
    m_nodeType = NodeType::SPRITE_ANIMATION;

    auto textureWidth = m_textureData->GetWidth();
    auto textureHeight = m_textureData->GetHeight();
    NOVA_ASSERT( textureWidth >= frameSize.GetX() && textureHeight >= frameSize.GetY(), std::string( "Spritesheet is to small" ) );

    int x = 0;
    int y = 0;
    int frameWidth = frameSize.GetX();
    int frameHeight = frameSize.GetY();
    for( int i = 0; i < amountOfKeyframes; ++i )
    {
        NOVA_ASSERT( y <= textureHeight, std::string( "Spritesheet is to small fot that amount of frames!" ) );
        m_keyframes.push_back( Rect( x, y, frameWidth, frameHeight ) );
        x += frameWidth;
        if( x + frameWidth > textureWidth )
        {
            x = 0;
            y += frameHeight;
        }
    }
}

void SpriteAnimation::Play( bool looped )
{
    m_play = true;
    m_looped = looped;
    m_finished = false;
}

void SpriteAnimation::Update( uint32_t dt )
{
    Node::Update( dt );

    if( m_play )
    {
        m_lastTimestamp += dt;
        if( m_lastTimestamp >= 1000 / m_fps )
        {
            m_lastTimestamp = 0;
            NextFrame();
            if( m_currentFrame == m_animations.at( m_currentAnimationName ).GetStartFrame() && !m_looped )
            {
                m_finished = true;
                Stop();
            }
        }
    }
}

void SpriteAnimation::SetAnimation( std::string name )
{
    m_currentAnimationName = name;
    m_currentFrame = m_animations.at( name ).GetStartFrame();
}

void SpriteAnimation::AddAnimation( std::string name, uint32_t startFrame, uint32_t endFrame )
{
    m_animations.emplace( name, AnimationInfo( startFrame, endFrame ) );
}

int SpriteAnimation::l_GetSize( luabridge::lua_State* L )
{
    luabridge::push( L, m_frameSize.GetX() );
    luabridge::push( L, m_frameSize.GetY() );
    return 2;
}

void SpriteAnimation::NextFrame()
{
    auto& currentAnimation = m_animations.at( m_currentAnimationName );
    if( m_currentFrame == currentAnimation.GetEndFrame() )
    {
        m_currentFrame = currentAnimation.GetStartFrame();
    }
    else
    {
        m_currentFrame++;
    }

    m_clipRect = m_keyframes[m_currentFrame];
}
