#ifndef __VECTOR_HPP__
#define __VECTOR_HPP__

#include <math.h>

namespace Math
{
    template<class T>
    class Vector2
    {
    public:
        Vector2()
            : m_x( 0 )
            , m_y( 0 )
        {
        }

        explicit Vector2( const T* vec )
            : m_x( v[0] )
            , m_y( v[1] )
        {
        }

        template<class C>
        explicit Vector2( const Vector2<C>& vec )
            : m_x( static_cast< T >( vec[0] ) )
            , m_y( static_cast< T >( vec[1] ) )
        {
        }

        Vector2( const T& v1, const T& v2 )
        {
            m_x = v1;
            m_y = v2;
        }

        const T GetX() const
        {
            return m_x;
        }

        const T GetY() const
        {
            return m_y;
        }

        void Set( T x, T y )
        {
            m_x = x;
            m_y = y;
        }

        T GetLength() const
        {
            return sqrt( m_x *m_x + m_y*m_y );
        }

        T& operator[]( int i )
        {
            return( ( T* )this )[i];
        }

        const T& operator[]( int i ) const
        {
            return ( ( T* )this )[i];
        }

        Vector2<T>& operator +=( const T& val )
        {
            m_x += val;
            m_y += val;
            return *this;
        }

        Vector2<T>& operator -=( const T& val )
        {
            m_x -= val;
            m_y -= val;
            return *this;
        }

        Vector2<T>& operator *=( const T& val )
        {
            m_x *= val;
            m_y *= val;
            return *this;
        }

        Vector2<T>& operator/=( const T& val )
        {
            T div = 1 / val;
            m_x *= div;
            m_y *= div;
            return *this;
        }

        Vector2<T>& operator+=( const Vector2<T>& vec )
        {
            m_x += vec[0];
            m_y += vec[1];
            return *this;
        }

        Vector2<T>& operator-=( const Vector2<T>& vec )
        {
            m_x -= vec[0];
            m_y -= vec[1];
            return *this;
        }

        Vector2<T>& operator*=( const Vector2<T>& vec )
        {
            m_x *= vec[0];
            m_y *= vec[1];
            return *this;
        }

        Vector2<T>& operator/=( const Vector2<T>& vec )
        {
            m_x /= vec[0];
            m_y /= vec[1];
            return *this;
        }

        Vector2<T> operator-() const
        {
            return Vector2<T>( -m_x, -m_y );
        }

    private:
        T m_x;
        T m_y;
    };

    template<>
    inline Vector2<int>& Vector2<int>::operator/=( const int& val )
    {
        m_x /= val;
        m_y /= val;
        return *this;
    }

    template<class T>
    inline Vector2<T> operator+( const Vector2<T>& vec1, const T& vec2 )
    {
        return Vector2<T>( vec1[0] + vec2, vec1[1] + vec2 );
    }

    template<class T>
    inline Vector2<T> operator-( const Vector2<T>& vec1, const T& vec2 )
    {
        return Vector<T>( vec1[0] - vec2, vec1[1] - vec2 );
    }

    template<class T>
    inline Vector2<T> operator*( const Vector2<T>& vec1, const T& vec2 )
    {
        return Vector2<T>( vec1[0] * vec2, vec1[1] * vec2 );
    }

    template<class T>
    inline Vector2<T> operator/( const Vector2<T>& vec1, const T& vec2 )
    {
        T div = 1 / vec2;
        return Vector2<T>( vec1[0] * div, vec1[1] * div );
    }

    template<>
    inline Vector2<int> operator/( const Vector2<int>& vec1, const int& vec2 )
    {
        return Vector2<int>( vec1[0] / vec2, vec1[1] / vec2 );
    }

    template<class T>
    inline Vector2<T> operator+( const T& vec1, const Vector2<T>& vec2 )
    {
        return Vector2<T>( vec1 + vec2[0], vec1 + vec2[1] );
    }

    template<class T>
    inline Vector2<T> operator-( const T& vec1, const Vector2<T>& vec2 )
    {
        return Vector2<T>( vec1 - vec2[0], vec1 - vec2[1] );
    }

    template<class T>
    inline Vector2<T> operator*( const T& vec1, const Vector2<T>& vec2 )
    {
        return Vector2<T>( vec1 * vec2[0], vec1 * vec2[1] );
    }

    template<class T>
    inline Vector2<T> operator/( const T& vec1, const Vector2<T>& vec2 )
    {
        return Vector2<T>( vec1 / vec2[0], vec1 / vec2[1] );
    }

    template<class T>
    inline Vector2<T> operator+( const Vector2<T>& vec1, const Vector2<T>& vec2 )
    {
        return Vector2<T>( vec1[0] + vec2[0], vec1[1] + vec2[1] );
    }

    template<class T>
    inline Vector2<T> operator-( const Vector2<T>& vec1, const Vector2<T>& vec2 )
    {
        return Vector2<T>( vec1[0] - vec2[0], vec1[1] - vec2[1] );
    }

    template<class T>
    inline Vector2<T> operator*( const Vector2<T>& vec1, const Vector2<T>& vec2 )
    {
        return Vector2<T>( vec1[0] * vec2[0], vec1[1] * vec2[1] );
    }

    template<class T>
    inline Vector2<T> operator/( const Vector2<T>& vec1, const Vector2<T>& vec2 )
    {
        return Vector2<T>( vec1[0] / vec2[0], vec1[1] / vec2[1] );
    }

    template<class T>
    inline bool operator==( const Vector2<T>& vec1, const Vector2<T>& vec2 )
    {
        return vec1[0] == vec2[0] && vec1[1] == vec2[1];
    }

    template<class T>
    inline bool operator!=( const Vector2<T>& vec1, const Vector2<T>& vec2 )
    {
        return !( vec1 == vec2 );
    }

    typedef Vector2<float> Vector2f;
    typedef Vector2<int> Vector2i;
    typedef Vector2<double> Vector2d;
}

#endif // !__VECTOR_HPP__
