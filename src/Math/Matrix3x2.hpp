#ifndef __MATRIX3X2_HPP__
#define __MATRIX3X2_HPP__

#include "Vector2.hpp"

namespace Math
{
    // [ 0 2 4 ]
    // [ 1 3 5 ]

    template<class T>
    class Matrix3x2
    {
    public:
        Matrix3x2()
        {
            m_matrix[0] = 1;
            m_matrix[1] = 0;
            m_matrix[2] = 0;
            m_matrix[3] = 1;
            m_matrix[4] = 0;
            m_matrix[5] = 0;
        }

        Matrix3x2( T* m )
        {
            memcpy( m_matrix, m, sizeof( m ) );
        }

        Matrix3x2( const T& v1, const T& v2, const T& v3, const T& v4, const T& v5, const T& v6 )
        {
            m_matrix[0] = v1;
            m_matrix[1] = v2;
            m_matrix[2] = v3;
            m_matrix[3] = v4;
            m_matrix[4] = v5;
            m_matrix[5] = v6;
        }

        T& operator[]( int i )
        {
            return m_matrix[i];
        }

        const T& operator[]( int i ) const
        {
            return m_matrix[i];
        }

        Matrix3x2& operator+=( const Vector2<T>& vec )
        {
            m_matrix[4] += vec.x;
            m_matrix[5] += vec.y;
            return *this;
        }

        Matrix3x2& operator-=( const Vector2<T>& vec )
        {
            m_matrix[4] -= vec.x;
            m_matrix[5] -= vec.y;
            return *this;
        }

    private:
        T m_matrix[6];
    };

    template<class T>
    inline Matrix3x2<T> operator+( const Matrix3x2<T>& m, const Vector2<T>& v )
    {
        return Matrix3x2<T>( m[0], m[1], m[2], m[3], m[4] + v.x, m[5] + v.y );
    }

    template<class T>
    inline Vector2<T> operator*( const Matrix3x2<T>& m, const Vector2<T>& v )
    {
        return Vector2<T>( m[0] * v[0] + m[2] * v[1] + m[4], m[1] * v[0] + m[3] * v[1] + m[5] );
    }

    template<class T>
    inline Matrix3x2<T> operator*( const Matrix3x2<T>& lhs, const Matrix3x2<T>& rhs )
    {
        return Matrix3x2<T>(
            lhs[0] * rhs[0] + lhs[2] * rhs[1],
            lhs[1] * rhs[0] + lhs[3] * rhs[1],
            lhs[0] * rhs[2] + lhs[2] * rhs[3],
            lhs[1] * rhs[2] + lhs[3] * rhs[3],
            lhs[0] * rhs[4] + lhs[2] * rhs[5] + lhs[4],
            lhs[1] * rhs[4] + lhs[3] * rhs[5] + lhs[5] );
    }

    typedef Matrix3x2<float> Matrix3x2f;
    typedef Matrix3x2<double> Matrix3x2d;
    typedef Matrix3x2<int> Matrix3x2i;
}

#endif // !__MATRIX3X2_HPP__
