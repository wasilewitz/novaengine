#include "Viewport.hpp"
#include "Application.hpp"

void Viewport::SetPosition( Vector2f position )
{
    m_position = position;
    App.GetRoot()->SetPosition( static_cast<Vector2f>( App.GetScreenSize() / 2 ) - position );
    App.GetOverlay()->SetPosition( Vector2f( 0,0 ) );
}

void Viewport::SetPosition( float x, float y )
{
    SetPosition( Vector2f( x, y ) );
}

void Viewport::l_SetPosition( float x, float y )
{
    SetPosition( x, y );
}

int Viewport::l_GetPosition( luabridge::lua_State * L )
{
    luabridge::push( L, m_position.GetX() );
    luabridge::push( L, m_position.GetY() );
    return 2;
}
