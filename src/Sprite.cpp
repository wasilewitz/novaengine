#include "Sprite.hpp"
#include "Application.hpp"

std::shared_ptr<Sprite> Sprite::Create( const std::string& id, const std::string& path, Rect clipRect )
{
    std::shared_ptr<Sprite> spritePtr ( new Sprite( id, path, clipRect ) );
    return spritePtr;
}

Sprite* Sprite::l_Create( luabridge::lua_State* L, std::string id, std::string path )
{
    Sprite* spritePtr = new Sprite( id, path );
    return spritePtr;
}

Sprite::Sprite( const std::string& id, const std::string& path, Rect clipRect )
    : Node( id )
    , m_sourceImagePath( path )
    , m_flip( SDL_FLIP_NONE )
    , m_clipRect( clipRect )
    , m_alpha( 255 )
{
    m_textureData = TextureManager::GetInstance().GetTexture( m_sourceImagePath );
    m_hasClipRect = clipRect != Rect();
    m_nodeType = NodeType::SPRITE;
}

void Sprite::SetClipRect( Rect clipRect )
{
    m_hasClipRect = true;
    m_clipRect = clipRect;
}

const Vector2f Sprite::GetSize() const
{
    if( m_hasClipRect )
    {
        return Vector2f( m_clipRect.GetSize().GetX(), m_clipRect.GetSize().GetY() );
    }
    else
    {
        return Vector2f( m_textureData->GetWidth(), m_textureData->GetHeight() );
    }
}

int Sprite::l_GetSize( luabridge::lua_State* L )
{
    auto size = GetSize();
    luabridge::push( L, size.GetX() );
    luabridge::push( L, size.GetY() );
    return 2;
}

void Sprite::Draw()
{
    NOVA_ASSERT( m_textureData != nullptr, "Texture data doesn't exist." );

    SDL_Point origin = { 0, 0 };
    Vector2f globalPosition = GetGlobalPosition();
    Vector2f globalScale = GetGlobalScale();

    SDL_Rect destinationRect;
    destinationRect.x = globalPosition.GetX();
    destinationRect.y = globalPosition.GetY();

    if( m_hasClipRect )
    {
        destinationRect.w = m_clipRect.GetSize().GetX() * globalScale.GetX();
        destinationRect.h = m_clipRect.GetSize().GetY() * globalScale.GetY();

        SDL_Rect sourceRect;
        sourceRect.x = m_clipRect.GetPosition().GetX();
        sourceRect.y = m_clipRect.GetPosition().GetY();
        sourceRect.w = m_clipRect.GetSize().GetX();
        sourceRect.h = m_clipRect.GetSize().GetY();

        SDL_SetTextureAlphaMod( m_textureData->GetTexture(), m_alpha );
        SDL_RenderCopyEx( App.GetRenderer(), m_textureData->GetTexture(), &sourceRect, &destinationRect, GetGlobalRotation(), &origin, m_flip );
    }
    else
    {
        destinationRect.w = m_textureData->GetWidth() * globalScale.GetX();
        destinationRect.h = m_textureData->GetHeight() * globalScale.GetY();
        SDL_SetTextureAlphaMod( m_textureData->GetTexture(), m_alpha );
        SDL_RenderCopyEx( App.GetRenderer(), m_textureData->GetTexture(), NULL, &destinationRect, GetGlobalRotation(), &origin, m_flip );
    }

    DrawChildren();
}

Sprite::~Sprite()
{
    TextureManager::GetInstance().ReleaseTexture( m_sourceImagePath );
}
