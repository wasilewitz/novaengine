#ifndef __CALLBACKS_MAP_HPP__
#define __CALLBACKS_MAP_HPP__

#include <map>
#include "CallbackBase.hpp"

template<class CallbackBase, class T2 >
class CallbacksMap
{
public:
    void Insert( const std::string& id, CallbackBase cb )
    {
        auto it = m_map.find( id );
        NOVA_ASSERT( it == m_map.end(), "Update listener with id: " + id + " already exists!" );
        m_map.emplace( id, cb );
    }

    void Execute( T2 val )
    {
        for( auto it = m_map.cbegin(), next_it = m_map.cbegin(); it != m_map.cend(); it = next_it )
        {
            next_it = it; ++next_it;
            if( ( *it ).second.IsOutOfDate() )
            {
                m_map.erase( it );
            }
            else
            {
                it->second.Execute( val );
            }
        }
    }

    void SetElementOutOfDate( const std::string& id )
    {
        auto it = m_map.find( id );
        if( it != m_map.end() )
        {
            ( *it ).second.SetOutOfDate();
        }
    }

    void Clear()
    {
        m_map.clear();
    }

private:
    std::map<const std::string, CallbackBase> m_map;
};

#endif // !__CALLBACKS_MAP_HPP__
