#ifndef __UTILITIES_HPP__
#define __UTILITIES_HPP__

#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#ifdef _WIN32
#include <Windows.h>
#endif
#include <SDL.h>
#include <cassert>
#include <algorithm>

static std::ofstream g_logFile;
static bool firstLog = true;

static double MilisecondsToSeconds( uint32_t ms )
{
    double seconds = static_cast<double>( ms ) / 1000;
    return seconds;
}

static void NOVA_LOG( const std::string& msg, bool addTime = true )
{
#ifdef _DEBUG
    std::ostringstream stream;

    if( addTime )
    {
        stream << "[" << MilisecondsToSeconds( SDL_GetTicks() ) << "s] ";
    }
    stream << msg << std::endl;

    if( firstLog )
    {
        firstLog = false;
        g_logFile.open( "../log.txt", std::ofstream::out );
    }
    else
    {
        g_logFile.open( "../log.txt", std::ofstream::out | std::ofstream::app );
    }
    g_logFile.write( stream.str().c_str(), stream.str().size() );
    g_logFile.close();

#ifdef _WIN32
    OutputDebugString( stream.str().c_str() );
#endif
    std::cout << stream.str();
#endif
}

static void NOVA_ERROR( const std::string& msg )
{
    NOVA_LOG( msg );
    assert( false );
}

static void NOVA_ASSERT( bool invariant, const std::string& msg )
{
    if( !invariant )
    {
        NOVA_ERROR( msg );
    }
}

#endif // !__UTILITIES_HPP__
