#ifndef __KEYBOARD_HANDLER_HPP__
#define __KEYBOARD_HANDLER_HPP__

#include <SDL.h>
#include "Utilities.hpp"
#include <functional>
#include <map>
#include <vector>
#include <LuaBridge.h>
#include "CallbackLua.hpp"
#include "Callback.hpp"
#include "CallbacksMap.hpp"

enum KeyCode
{
    KEY_ARROW_LEFT = SDL_SCANCODE_LEFT,
    KEY_ARROW_RIGHT = SDL_SCANCODE_RIGHT,
    KEY_ARROW_UP = SDL_SCANCODE_UP,
    KEY_ARROW_DOWN = SDL_SCANCODE_DOWN,
    KEY_Q = SDL_SCANCODE_Q,
    KEY_W = SDL_SCANCODE_W,
    KEY_E = SDL_SCANCODE_E,
    KEY_R = SDL_SCANCODE_R,
    KEY_T = SDL_SCANCODE_T,
    KEY_Y = SDL_SCANCODE_Y,
    KEY_U = SDL_SCANCODE_U,
    KEY_I = SDL_SCANCODE_I,
    KEY_O = SDL_SCANCODE_O,
    KEY_P = SDL_SCANCODE_P,
    KEY_A = SDL_SCANCODE_A,
    KEY_S = SDL_SCANCODE_S,
    KEY_D = SDL_SCANCODE_D,
    KEY_F = SDL_SCANCODE_F,
    KEY_G = SDL_SCANCODE_G,
    KEY_H = SDL_SCANCODE_H,
    KEY_J = SDL_SCANCODE_J,
    KEY_K = SDL_SCANCODE_K,
    KEY_L = SDL_SCANCODE_L,
    KEY_Z = SDL_SCANCODE_Z,
    KEY_X = SDL_SCANCODE_X,
    KEY_C = SDL_SCANCODE_C,
    KEY_V = SDL_SCANCODE_V,
    KEY_B = SDL_SCANCODE_B,
    KEY_N = SDL_SCANCODE_N,
    KEY_M = SDL_SCANCODE_M,
    KEY_0 = SDL_SCANCODE_0,
    KEY_1 = SDL_SCANCODE_1,
    KEY_2 = SDL_SCANCODE_2,
    KEY_3 = SDL_SCANCODE_3,
    KEY_4 = SDL_SCANCODE_4,
    KEY_5 = SDL_SCANCODE_5,
    KEY_6 = SDL_SCANCODE_6,
    KEY_7 = SDL_SCANCODE_7,
    KEY_8 = SDL_SCANCODE_8,
    KEY_9 = SDL_SCANCODE_9,
    SPACE = SDL_SCANCODE_SPACE
};

#define KeyboardEventListener KeyboardHandler::GetInstance()

class KeyboardHandler
{
public:
    static KeyboardHandler& GetInstance()
    {
        static KeyboardHandler instance;
        return instance;
    }

    void HandleEvent( SDL_Event& e );

    luabridge::LuaRef l_GetKeyboardMap() { return m_keyboardMap; }

    /*Example:
    KeyboardEventListener.RegisterOnKeyPressedListener( "listenerId", [&]( KeyCode keyCode ){
        switch( keyCode )
        {
            case KEY_ARROW_DOWN:
                NOVA_LOG( LOG_INFO, "Down pressed!" );
                break;
            case KEY_ARROW_UP:
                NOVA_LOG( LOG_INFO, "Up pressed!" );
                break;
        }
    } );*/
    void RegisterOnKeyPressedListener( std::string id, std::function<void(KeyCode keyCode)> func );
    void UnregisterOnKeyPressedListener( const std::string listenerId );
    void l_RegisterOnKeyPressedListener( const std::string listenerId, luabridge::LuaRef func );
    void l_UnregisterOnKeyPressedListener( const std::string listenerId );
    void UnregisterOnKeyPressedListeners();

    /*
    Example:
    KeyboardEventListener.RegisterOnKeyBeingPressedListener( "id", []( KeyCode keyCode, const Uint8* keyboardState ){
        if( keyboardState[KeyCode::KEY_ARROW_DOWN] )
        {
            NOVA_LOG( LOG_INFO, "Down is being pressed!" );
        }
        if( keyboardState[KeyCode::KEY_ARROW_UP] )
        {
            NOVA_LOG( LOG_INFO, "Up is being pressed!" );
        }
    } );*/
    void RegisterOnKeyBeingPressedListener( std::string id, std::function<void( const Uint8* keyboardState )> func );
    void UnregisterOnKeyBeingPressedListener( std::string listenerId );
    void l_RegisterOnKeyBeingPressedListener( const std::string listenerId, luabridge::LuaRef func );
    void l_UnregisterOnKeyBeingPressedListener( const std::string listenerId );
    void UnregisterOnKeyBeingPressedListeners();

    void Release();

private:
    KeyboardHandler();

    void MapKeyboardKeysToLuaTable();

    const Uint8* m_keyboardState;
    luabridge::LuaRef m_keyboardMap;

    CallbacksMap<Callback<KeyCode>, KeyCode> m_keysPressedCallbacks;
    CallbacksMap<CallbackLua<int>, int> m_luaKeysPressedCallbacks;
    CallbacksMap<Callback<const Uint8*>, const Uint8*> m_keysBeingPressedCallbacks;
    CallbacksMap<CallbackLua<luabridge::LuaRef>, luabridge::LuaRef> m_luaKeysBeingPressedCallbacks;
};

#endif // !__KEYBOARD_HANDLER_HPP__
