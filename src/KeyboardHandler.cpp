#include "KeyboardHandler.hpp"
#include "Application.hpp"

KeyboardHandler::KeyboardHandler()
    : m_keyboardMap( luabridge::newTable(App.GetLuaState()) )
{
    m_keyboardState = SDL_GetKeyboardState( NULL );

    /* Create map of keyboard keys as a lua table. */
    MapKeyboardKeysToLuaTable();

    App.RegisterUpdateCallback( "KeyboardHandlerUpdate", [&]( uint32_t dt ) {
        m_keysBeingPressedCallbacks.Execute( m_keyboardState );

        luabridge::LuaRef keysBeingPressedMap = luabridge::newTable( App.GetLuaState() );
        for( luabridge::Iterator iter( m_keyboardMap ); !iter.isNil(); ++iter )
        {
            luabridge::LuaRef value = *iter;
            if( m_keyboardState[value.cast<int>()] )
            {
                keysBeingPressedMap[iter.key()] = true;
            }
        }

        m_luaKeysBeingPressedCallbacks.Execute( keysBeingPressedMap );
    } );
}

void KeyboardHandler::MapKeyboardKeysToLuaTable()
{
    m_keyboardMap["KEY_ARROW_LEFT"] = (int)KeyCode::KEY_ARROW_LEFT;
    m_keyboardMap["KEY_ARROW_RIGHT"] = (int)KeyCode::KEY_ARROW_RIGHT;
    m_keyboardMap["KEY_ARROW_UP"] = (int)KeyCode::KEY_ARROW_UP;
    m_keyboardMap["KEY_ARROW_DOWN"] = (int)KeyCode::KEY_ARROW_DOWN;
    m_keyboardMap["KEY_Q"] = (int)KeyCode::KEY_Q;
    m_keyboardMap["KEY_W"] = (int)KeyCode::KEY_W;
    m_keyboardMap["KEY_E"] = (int)KeyCode::KEY_E;
    m_keyboardMap["KEY_R"] = (int)KeyCode::KEY_R;
    m_keyboardMap["KEY_T"] = (int)KeyCode::KEY_T;
    m_keyboardMap["KEY_Y"] = (int)KeyCode::KEY_Y;
    m_keyboardMap["KEY_U"] = (int)KeyCode::KEY_U;
    m_keyboardMap["KEY_I"] = (int)KeyCode::KEY_I;
    m_keyboardMap["KEY_O"] = (int)KeyCode::KEY_O;
    m_keyboardMap["KEY_P"] = (int)KeyCode::KEY_P;
    m_keyboardMap["KEY_A"] = (int)KeyCode::KEY_A;
    m_keyboardMap["KEY_S"] = (int)KeyCode::KEY_S;
    m_keyboardMap["KEY_D"] = (int)KeyCode::KEY_D;
    m_keyboardMap["KEY_F"] = (int)KeyCode::KEY_F;
    m_keyboardMap["KEY_G"] = (int)KeyCode::KEY_G;
    m_keyboardMap["KEY_H"] = (int)KeyCode::KEY_H;
    m_keyboardMap["KEY_J"] = (int)KeyCode::KEY_J;
    m_keyboardMap["KEY_K"] = (int)KeyCode::KEY_K;
    m_keyboardMap["KEY_L"] = (int)KeyCode::KEY_L;
    m_keyboardMap["KEY_Z"] = (int)KeyCode::KEY_Z;
    m_keyboardMap["KEY_X"] = (int)KeyCode::KEY_X;
    m_keyboardMap["KEY_C"] = (int)KeyCode::KEY_C;
    m_keyboardMap["KEY_V"] = (int)KeyCode::KEY_V;
    m_keyboardMap["KEY_B"] = (int)KeyCode::KEY_B;
    m_keyboardMap["KEY_N"] = (int)KeyCode::KEY_N;
    m_keyboardMap["KEY_M"] = (int)KeyCode::KEY_M;
    m_keyboardMap["KEY_0"] = (int)KeyCode::KEY_0;
    m_keyboardMap["KEY_1"] = (int)KeyCode::KEY_1;
    m_keyboardMap["KEY_2"] = (int)KeyCode::KEY_2;
    m_keyboardMap["KEY_3"] = (int)KeyCode::KEY_3;
    m_keyboardMap["KEY_4"] = (int)KeyCode::KEY_4;
    m_keyboardMap["KEY_5"] = (int)KeyCode::KEY_5;
    m_keyboardMap["KEY_6"] = (int)KeyCode::KEY_6;
    m_keyboardMap["KEY_7"] = (int)KeyCode::KEY_7;
    m_keyboardMap["KEY_8"] = (int)KeyCode::KEY_8;
    m_keyboardMap["KEY_9"] = (int)KeyCode::KEY_9;
    m_keyboardMap["KEY_SPACE"] = (int)KeyCode::SPACE;
};

void KeyboardHandler::HandleEvent( SDL_Event& e )
{
    if( e.type == SDL_KEYUP )
    {
        m_keysPressedCallbacks.Execute( (KeyCode)e.key.keysym.scancode );
        m_luaKeysPressedCallbacks.Execute( (int)(KeyCode)e.key.keysym.scancode );
    }
}

void KeyboardHandler::RegisterOnKeyPressedListener( const std::string id, std::function<void(KeyCode keyCode)> func )
{
    m_keysPressedCallbacks.Insert( id, func );
}

void KeyboardHandler::UnregisterOnKeyPressedListener( const std::string listenerId )
{
    m_keysPressedCallbacks.SetElementOutOfDate( listenerId );
}

void KeyboardHandler::l_RegisterOnKeyPressedListener( const std::string listenerId, luabridge::LuaRef func )
{
    m_luaKeysPressedCallbacks.Insert( listenerId, func );
}

void KeyboardHandler::l_UnregisterOnKeyPressedListener( const std::string listenerId )
{
    m_luaKeysPressedCallbacks.SetElementOutOfDate( listenerId );
}

void KeyboardHandler::RegisterOnKeyBeingPressedListener( std::string id, std::function<void( const Uint8* keyboardState )> func )
{
    m_keysBeingPressedCallbacks.Insert( id, func );
}

void KeyboardHandler::UnregisterOnKeyBeingPressedListener( std::string listenerId )
{
    m_keysBeingPressedCallbacks.SetElementOutOfDate( listenerId );
}

void KeyboardHandler::l_RegisterOnKeyBeingPressedListener( const std::string listenerId, luabridge::LuaRef func )
{
    m_luaKeysBeingPressedCallbacks.Insert( listenerId, func );
}

void KeyboardHandler::l_UnregisterOnKeyBeingPressedListener( const std::string listenerId )
{
    m_luaKeysBeingPressedCallbacks.SetElementOutOfDate( listenerId );
}

void KeyboardHandler::UnregisterOnKeyPressedListeners()
{
    m_keysPressedCallbacks.Clear();
    m_luaKeysPressedCallbacks.Clear();
}

void KeyboardHandler::UnregisterOnKeyBeingPressedListeners()
{
    m_keysBeingPressedCallbacks.Clear();
    m_luaKeysBeingPressedCallbacks.Clear();
}

void KeyboardHandler::Release()
{
    UnregisterOnKeyBeingPressedListeners();
    UnregisterOnKeyPressedListeners();
    App.UnregisterUpdateCallback( "KeyboardHandlerUpdate" );
}
