#include "TextureResource.hpp"

#include "Application.hpp"

TextureResource::TextureResource( const std::string& path )
    : m_referenceCount( 0 )
    , m_path( path )
{
    SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
    NOVA_ASSERT( loadedSurface != nullptr, "Unable to load image " + path + ". SDL_image Error: " + IMG_GetError() );

    m_texture = SDL_CreateTextureFromSurface( App.GetRenderer(), loadedSurface );
    NOVA_ASSERT( m_texture != nullptr, "Unable to create texture from " + path + ". SDL Error: " + SDL_GetError() );

    SDL_FreeSurface( loadedSurface );

    m_textureData.reset( new TextureData( m_texture ) );
    m_referenceCount = 1;
}

TextureResource::~TextureResource()
{
    m_textureData = nullptr;

    if( m_texture )
    {
        SDL_DestroyTexture( m_texture );
        m_texture = nullptr;
    }
}

void TextureResource::AddReference()
{
    m_referenceCount++;
}

void TextureResource::RemoveReference()
{
    NOVA_ASSERT( m_referenceCount > 0, "Reference count for " + m_path + " cannot be lower than 0." );
    m_referenceCount--;
}
