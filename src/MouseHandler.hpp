#ifndef __MOUSE_HANDLER_HPP__
#define __MOUSE_HANDLER_HPP__

#include <SDL.h>
#include "Utilities.hpp"
#include <functional>
#include <map>
#include "Application.hpp"

#define MouseEventListener MouseHandler::GetInstance()

enum MouseButton {
    LEFT = 0,
    MIDDLE = 1,
    RIGHT = 2
};

class MouseHandler
{
public:
    static MouseHandler& GetInstance()
    {
        static MouseHandler instance;
        return instance;
    }

    void HandleEvent( SDL_Event& e );

    luabridge::LuaRef l_GetMouseButtonsMap() { return m_mouseButtonsMap; }

    void RegisterOnMouseMovedListener( std::string id, std::function<void( int posX, int posY )> func );
    void l_RegisterOnMouseMovedListener( const std::string listenerId, luabridge::LuaRef func );
    void UnregisterOnMouseMovedListener( const std::string listenerId );
    void l_UnregisterOnMouseMovedListener( const std::string listenerId );
    void UnregisterOnMouseMovedListeners();

    void RegisterOnMouseButtonUpListener( std::string id, std::function<void( MouseButton button, int posX, int posY )> func );
    void l_RegisterOnMouseButtonUpListener( const std::string listenerId, luabridge::LuaRef func );
    void UnregisterOnMouseButtonUpListener( const std::string listenerId );
    void l_UnregisterOnMouseButtonUpListener( const std::string listenerId );
    void UnregisterOnMouseButtonUpListeners();

    void RegisterOnMouseButtonDownListener( std::string id, std::function<void( MouseButton button, int posX, int posY )> func );
    void l_RegisterOnMouseButtonDownListener( const std::string listenerId, luabridge::LuaRef func );
    void UnregisterOnMouseButtonDownListener( const std::string listenerId );
    void l_UnregisterOnMouseButtonDownListener( const std::string listenerId );
    void UnregisterOnMouseButtonDownListeners();

    void RegisterOnMouseButtonsBeingPressedListener( std::string id, std::function<void( uint32_t dt, bool* mouseButtonsState, int posX, int posY )> func );
    void l_RegisterOnMouseButtonsBeingPressedListener( const std::string listenerId, luabridge::LuaRef func );
    void UnregisterOnMouseButtonsBeingPressedListener( const std::string listenerId );
    void l_UnregisterOnMouseButtonsBeingPressedListener( const std::string listenerId );
    void UnregisterOnMouseButtonsBeingPressedListeners();

    Vector2i GetMappedMousePointerPos();

    void Release();

private:
    MouseHandler();

    void MapMouseButtonsToLuaTable();

    std::map<const std::string, std::function<void( int posX, int posY )>> m_mouseMovedListeners;
    std::map<const std::string, luabridge::LuaRef> m_luaMouseMovedListeners;
    std::map<const std::string, std::function<void( MouseButton button, int posX, int posY )>> m_mouseButtonDownListeners;
    std::map<const std::string, luabridge::LuaRef> m_luaMouseButtonDownListeners;
    std::map<const std::string, std::function<void( MouseButton button, int posX, int posY )>> m_mouseButtonUpListeners;
    std::map<const std::string, luabridge::LuaRef> m_luaMouseButtonUpListeners;
    std::map<const std::string, std::function<void( uint32_t dt, bool* mouseButtonsState, int posX, int posY )>> m_mouseButtonsBeingPressedListeners;
    std::map<const std::string, luabridge::LuaRef> m_luaMouseButtonsBeingPressedListeners;

    bool m_mouseButtonsState[3] = { false };

    luabridge::LuaRef m_mouseButtonsMap;
};

#endif // !__MOUSE_HANDLER_HPP__
