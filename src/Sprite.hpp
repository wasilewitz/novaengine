#ifndef __SPRITE_HPP__
#define __SPRITE_HPP__

#include<memory>
#include <string>
#include "Utilities.hpp"
#include "Node.hpp"
#include "Rect.hpp"
#include <LuaBridge.h>
#include "TextureManager.hpp"

class Sprite : public Node
{
public:
    static std::shared_ptr<Sprite> Create( const std::string& id, const std::string& path, Rect clipRect = Rect() );
    //TODO: Add possibility to pass clipRect as default argument
    static Sprite* l_Create( luabridge::lua_State* L, std::string id, std::string path );
    /*
    SDL_FLIP_NONE - do not flip
    SDL_FLIP_HORIZONTAL - flip horizontally
    SDL_FLIP_VERTICAL - flip vertically
    SDL_FLIP_HORIZONTAL | SDL_FLIP_VERTICAL - flip horizontally and vertically
    */
    void SetFlip( SDL_RendererFlip flip ) { m_flip = flip; }
    void SetClipRect( Rect clipRect );
    const Vector2f GetSize() const;
    int l_GetSize( luabridge::lua_State* L );
    void SetAlpha( int alpha ) { m_alpha = alpha; }

    /* Do not use this method. Drawing sprite is handled by app */
    void Draw();
    virtual ~Sprite();

protected:
    Sprite( const std::string& id, const std::string& path, Rect clipRect = Rect() );
    TextureData* m_textureData;
    std::string m_sourceImagePath;
    SDL_RendererFlip m_flip;
    bool m_hasClipRect;
    Rect m_clipRect;
    int m_alpha;
};

#endif // !__SPRITE_HPP__
