#include "FontsManager.hpp"
#include "tinyxml2.h"
#include "TextureManager.hpp"

std::shared_ptr<Font> FontsManager::GetFont( const std::string& path )
{
    auto fontIt = m_fonts.find( path );
    if( fontIt == m_fonts.end() )
    {
        auto font = std::shared_ptr<Font>( new Font( path ) );
        m_fonts.insert( std::make_pair( path, font ) );
        return font;
    }
    else
    {
        return fontIt->second;
    }
}

Font::Font( const std::string& path )
{
    tinyxml2::XMLDocument fontFile;
    auto ret = fontFile.LoadFile( path.c_str() );
    NOVA_ASSERT( ret == 0, "Font " + path + " doesnt exist." );
    auto root = fontFile.RootElement();

    m_sourceImagePath = root->Attribute( "sourceImage" );

    for( auto anim = root->FirstChildElement( "char" ); anim; anim = anim->NextSiblingElement( "char" ) )
    {
        int asciiCode = std::stoi( anim->Attribute( "ascii" ) );
        Rect rect( std::stoi( anim->Attribute( "x" ) ), std::stoi( anim->Attribute( "y" ) ), std::stoi( anim->Attribute( "w" ) ), std::stoi( anim->Attribute( "h" ) ) );
        m_characters.insert( std::make_pair( asciiCode, std::shared_ptr<FontCharacter>( new FontCharacter( asciiCode, rect ) ) ) );
    }
}

Rect& Font::GetCharacterRect( int asciiCode )
{
    auto fontCharacterIt = m_characters.find( asciiCode );
    NOVA_ASSERT( fontCharacterIt != m_characters.end(), "Required character doesn't exists in this font." );
    return fontCharacterIt->second->m_rect;
}
