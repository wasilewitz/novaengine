#include "Application.hpp"

/* 16ms. Simulate 60 fps */
static const uint32_t STEP = 16;

/* Default resolutions for 4:3, 16:9 and 16:10 ratios.
   Screen will be scaled to these resolutions depending on ratio. */
static const Vector2i DEFAULT_RESOLUTION_16_9 = Vector2i( 1920, 1080 );
static const Vector2i DEFAULT_RESOLUTION_16_10 = Vector2i( 1920, 1200 );
static const Vector2i DEFAULT_RESOLUTION_4_3 = Vector2i( 1920, 1440 );

void Application::Init( const std::string& windowTitle, uint32_t windowWidth, uint32_t windowHeight )
{
    if( SDL_Init( SDL_INIT_TIMER | SDL_INIT_VIDEO ) < 0 )
    {
        NOVA_ERROR( "SDL could not initialize! SDL_Error: " + std::string( SDL_GetError() ) );
    }
    else if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
    {
        NOVA_ERROR( "SDL_mixer could not initialize! SDL_mixer Error: " + std::string( Mix_GetError() ) );
    }
    else
    {
        NOVA_LOG( "SDL Init completed." );
    }

    SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" );

    m_window = SDL_CreateWindow( windowTitle.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowWidth, windowHeight, SDL_WINDOW_SHOWN );
    if( m_window == NULL )
    {
        NOVA_ERROR( "Window could not be created! SDL_Error: " + std::string( SDL_GetError() ) );
    }
    else
    {
        m_renderer = SDL_CreateRenderer( m_window, -1, SDL_RENDERER_ACCELERATED );
        if( m_renderer == NULL )
        {
            NOVA_ERROR( "Renderer could not be created! SDL_Error: " + std::string( SDL_GetError() ) );
        }
        SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
    }

    float ratio = static_cast<float>( max( windowWidth, windowHeight ) ) / static_cast<float>( min( windowWidth, windowHeight ) );
    if( ratio >= 1.7f )
    {
        m_screenSize = DEFAULT_RESOLUTION_16_9;
    }
    else if( ratio >= 1.6f )
    {
        m_screenSize = DEFAULT_RESOLUTION_16_10;
    }
    else
    {
        m_screenSize = DEFAULT_RESOLUTION_4_3;
    }

    bool isVertical = windowWidth < windowHeight;
    if( isVertical )
    {
        m_screenSize.Set( m_screenSize.GetY(), m_screenSize.GetX() );
    }

    SDL_RenderSetLogicalSize( m_renderer, m_screenSize.GetX(), m_screenSize.GetY() );
    m_screenResToWindowSizeRatio = static_cast<float>( m_screenSize.GetX() ) / static_cast<float>( windowWidth );

    m_root = std::shared_ptr<Node>( new Node( "root" ) );
    m_overlay = std::shared_ptr<Node>( new Node( "overlay" ) );

    m_L = luabridge::luaL_newstate();
    luabridge::luaL_openlibs( m_L );

    AddLuaWrappers();
}

void Application::MainLoop()
{
    bool quit = false;
    SDL_Event e;
    uint32_t frameTime;
    uint32_t lastFrameTime = 0;
    uint32_t dt = 0;
    uint32_t fps = 0;
    uint32_t fpsTimer = SDL_GetTicks();

    while( !quit )
    {
        frameTime = SDL_GetTicks();

        /* Handle events on queue */
        while( SDL_PollEvent( &e ) != 0 )
        {
            if( e.type == SDL_QUIT )
            {
                quit = true;
            }
            KeyboardHandler::GetInstance().HandleEvent( e );
            MouseHandler::GetInstance().HandleEvent( e );
        }

        dt = frameTime - lastFrameTime;
        lastFrameTime = frameTime;

        /* Counting fps */
        if( frameTime > fpsTimer + 1000 )
        {
            fpsTimer = SDL_GetTicks();
            fps = 0;
        }
        else
        {
            fps++;
        }

        Update( dt );
        RenderFrame();

        if( ( SDL_GetTicks() - frameTime ) < STEP )
        {
            SDL_Delay( STEP - ( SDL_GetTicks() - frameTime ) );
        }
    }
}

void Application::Quit()
{
    NOVA_LOG( "SDL Quit" );
    KeyboardHandler::GetInstance().Release();
    MouseHandler::GetInstance().Release();
    UnregisterAllUpdateCallbacks();
    m_root->RemoveAllChildren();
    m_overlay->RemoveAllChildren();
    SDL_DestroyRenderer( m_renderer );
    SDL_DestroyWindow( m_window );
    IMG_Quit();
    SDL_Quit();
}

void Application::RegisterUpdateCallback( const std::string& id, std::function<void( uint32_t )> updateCallback )
{
    m_updateCallbacks.Insert( id, updateCallback );
}

void Application::l_RegisterUpdateCallback( const std::string& id, luabridge::LuaRef func )
{
    m_luaUpdateCallbacks.Insert( id, func );
}

void Application::UnregisterUpdateCallback( const std::string& id )
{
    m_updateCallbacks.SetElementOutOfDate( id );
}

void Application::l_UnregisterUpdateCallback( const std::string& id )
{
    m_luaUpdateCallbacks.SetElementOutOfDate( id );
}

void Application::UnregisterAllUpdateCallbacks()
{
    m_updateCallbacks.Clear();
    m_luaUpdateCallbacks.Clear();
}

int Application::l_GetScreenSize( luabridge::lua_State* L )
{
    luabridge::push( L, m_screenSize.GetX() );
    luabridge::push( L, m_screenSize.GetY() );
    return 2;
}

void Application::HandleTouch( Vector2i& screenTouchPos )
{
    auto& viewportPosition = Viewport::GetInstance().GetPosition();
    auto& screenSize = GetScreenSize();
    Vector2f worldTouchPosition( screenTouchPos.GetX(), screenTouchPos.GetY() );
    m_root->HandleTouch( worldTouchPosition );
    m_overlay->HandleTouch( worldTouchPosition );
}

void Application::RenderFrame()
{
    SDL_RenderClear( m_renderer );

    m_root->Draw();
    m_overlay->Draw();

    SDL_RenderPresent( m_renderer );
}

void Application::Update( uint32_t dt )
{
    if( dt >= 500 )
    {
        NOVA_LOG( "SLOW FRAME: dt = " + std::to_string( dt ) );
    }

    m_updateCallbacks.Execute( dt );
    m_luaUpdateCallbacks.Execute( dt );

    m_root->Update( dt );
    m_overlay->Update( dt );
}

void Application::AddLuaWrappers()
{
    //KeyboardHandler
    luabridge::getGlobalNamespace( m_L )
        .beginClass<KeyboardHandler>( "KeyboardHandler" )
        .addFunction( "GetKeyboardMap", &KeyboardHandler::l_GetKeyboardMap )
        .addFunction( "RegisterOnKeyPressedListener", &KeyboardHandler::l_RegisterOnKeyPressedListener )
        .addFunction( "UnregisterOnKeyPressedListener", &KeyboardHandler::l_UnregisterOnKeyPressedListener )
        .addFunction( "RegisterOnKeysBeingPressedListener", &KeyboardHandler::l_RegisterOnKeyBeingPressedListener )
        .addFunction( "UnregisterOnKeysBeingPressedListener", &KeyboardHandler::l_UnregisterOnKeyBeingPressedListener )
        .endClass();

    //KeyboardHandler instance
    luabridge::push( m_L, &KeyboardEventListener );
    lua_setglobal( m_L, "KeyboardHandler" );

    //MouseHandler
    luabridge::getGlobalNamespace( m_L )
        .beginClass<MouseHandler>( "MouseHandler" )
        .addFunction( "GetMouseButtonsMap", &MouseHandler::l_GetMouseButtonsMap )
        .addFunction( "RegisterOnMouseMovedListener", &MouseHandler::l_RegisterOnMouseMovedListener )
        .addFunction( "UnregisterOnMouseMovedListener", &MouseHandler::l_UnregisterOnMouseMovedListener )
        .addFunction( "RegisterOnMouseButtonUpListener", &MouseHandler::l_RegisterOnMouseButtonUpListener )
        .addFunction( "UnregisterOnMouseButtonUpListener", &MouseHandler::l_UnregisterOnMouseButtonUpListener )
        .addFunction( "RegisterOnMouseButtonDownListener", &MouseHandler::l_RegisterOnMouseButtonDownListener )
        .addFunction( "UnregisterOnMouseButtonDownListener", &MouseHandler::l_UnregisterOnMouseButtonDownListener )
        .addFunction( "RegisterOnMouseButtonsBeingPressedListener", &MouseHandler::l_RegisterOnMouseButtonsBeingPressedListener )
        .addFunction( "UnregisterOnMouseButtonsBeingPressedListener", &MouseHandler::l_UnregisterOnMouseButtonsBeingPressedListener )
        .endClass();

    //MouseHandler instance
    luabridge::push( m_L, &MouseEventListener );
    lua_setglobal( m_L, "MouseHandler" );

    //Application
    luabridge::getGlobalNamespace( m_L )
        .beginClass<Application>( "Application" )
        .addFunction( "RegisterUpdateCallback", &Application::l_RegisterUpdateCallback )
        .addFunction( "UnregisterUpdateCallback", &Application::l_UnregisterUpdateCallback )
        .addFunction( "Print", &Application::l_Print )
        .addCFunction( "GetScreenSize", &Application::l_GetScreenSize )
        .addFunction( "ShowCursor", &Application::ShowCursor )
        .endClass();

    //Application instance
    luabridge::push( m_L, &App );
    lua_setglobal( m_L, "App" );

    //Camera
    luabridge::getGlobalNamespace( m_L )
        .beginClass<Viewport>( "Viewport" )
        .addFunction( "SetPosition", &Viewport::l_SetPosition )
        .addCFunction( "GetPosition", &Viewport::l_GetPosition )
        .endClass();

    //Camera instance
    luabridge::push( m_L, &Viewport::GetInstance() );
    lua_setglobal( m_L, "Camera" );

    //Node
    luabridge::getGlobalNamespace( m_L )
        .beginClass<Node>( "Node" )
        .addStaticFunction( "Create", &Node::l_Create )
        .addFunction( "AddChild", &Node::l_AddChild )
        .addFunction( "GetChild", &Node::l_GetChild )
        .addFunction( "SetParent", &Node::SetParent )
        .addFunction( "GetID", &Node::GetID )
        .addFunction( "RemoveAllChildren", &Node::RemoveAllChildren )
        .addFunction( "Remove", &Node::Remove )
        .addCFunction( "GetPosition", &Node::l_GetPosition )
        .addFunction( "SetPosition", &Node::l_SetPosition )
        .addFunction( "Move", &Node::l_Move )
        .addCFunction( "GetOrigin", &Node::l_GetOrigin )
        .addFunction( "SetOrigin", &Node::l_SetOrigin )
        .addCFunction( "GetScale", &Node::l_GetScale )
        .addFunction( "SetScale", &Node::l_SetScale )
        .addFunction( "GetRotation", &Node::GetRotation )
        .addFunction( "SetRotation", &Node::SetRotation )
        .addFunction( "EnableTouch", &Node::l_EnableTouch )
        .addFunction( "DisableTouch", &Node::l_DisableTouch )
        .endClass();

    //Sprite
    luabridge::getGlobalNamespace( m_L )
        .deriveClass<Sprite, Node>( "Sprite" )
        .addStaticFunction( "Create", &Sprite::l_Create )
        .addCFunction( "GetSize", &Sprite::l_GetSize )
        .addFunction( "SetAlpha", &Sprite::SetAlpha )
        .endClass();

    //SpriteAnimation
    luabridge::getGlobalNamespace( m_L )
        .deriveClass<SpriteAnimation, Sprite>( "SpriteAnimation" )
        .addStaticFunction( "LoadFromXML", &SpriteAnimation::l_LoadFromXML )
        .addFunction( "Play", &SpriteAnimation::Play )
        .addFunction( "Stop", &SpriteAnimation::Stop )
        .addFunction( "SetAnimation", &SpriteAnimation::SetAnimation )
        .addFunction( "AddAnimation", &SpriteAnimation::AddAnimation )
        .addFunction( "GetAnimation", &SpriteAnimation::GetAnimation )
        .addCFunction( "GetSize", &SpriteAnimation::l_GetSize )
        .addFunction( "IsFinished", &SpriteAnimation::IsFinished )
        .endClass();

    //Text
    luabridge::getGlobalNamespace( m_L )
        .deriveClass<Text, Node>( "Text" )
        .addStaticFunction( "Create", &Text::l_Create )
        .addFunction( "SetText", &Text::SetText )
        .addCFunction( "GetSize", &Text::l_GetSize )
        .endClass();

    //Root
    luabridge::push( m_L, m_root.get() );
    lua_setglobal( m_L, "root" );

    //Overlay
    luabridge::push( m_L, m_overlay.get() );
    lua_setglobal( m_L, "overlay" );

    //AudioManager
    luabridge::getGlobalNamespace( m_L )
        .beginClass<AudioManager>( "AudioManagerClass" )
        .addFunction( "Play", &AudioManager::Play )
        .endClass();

    //AudioManager instance
    luabridge::push( m_L, &AudioManager::GetInstance() );
    lua_setglobal( m_L, "AudioManager" );
}
