#ifndef __APPLICATION_HPP__
#define __APPLICATION_HPP__

#include <iostream>
#include <stdio.h>
#include <SDL.h>
#include <stdio.h>
#include <functional>
#include <vector>
#include "Utilities.hpp"
#include "Sprite.hpp"
#include "SpriteAnimation.hpp"
#include "KeyboardHandler.hpp"
#include "MouseHandler.hpp"
#include "Viewport.hpp"
#include "LuaWrapper.hpp"
#include "TextureManager.hpp"
#include "AudioManager.hpp"
#include "Text.hpp"
#include "CallbackLua.hpp"
#include "Callback.hpp"
#include "CallbacksMap.hpp"

#include <LuaBridge.h>
extern "C" {
# include "lua.h"
# include "lauxlib.h"
# include "lualib.h"
}

#define App Application::GetInstance()

class Application
{
public:
    static Application& GetInstance()
    {
        static Application instance;
        return instance;
    }

    void Init( const std::string& windowTitle, uint32_t windowWidth, uint32_t windowHeight );
    void MainLoop();
    void Quit();

    SDL_Renderer* GetRenderer() { return m_renderer; }
    std::shared_ptr<Node> GetRoot() { return m_root; }
    std::shared_ptr<Node> GetOverlay() { return m_overlay; }

    /* Example:
    App.RegisterUpdateCallback( "id", [&]( uint32_t dt ) {
        Update( dt );
    } ); */
    void RegisterUpdateCallback( const std::string& id, std::function<void( uint32_t )> updateCallback );
    void l_RegisterUpdateCallback( const std::string& id, luabridge::LuaRef func );
    void UnregisterUpdateCallback( const std::string& id );
    void l_UnregisterUpdateCallback( const std::string& id );
    void UnregisterAllUpdateCallbacks();

    Vector2i GetScreenSize() const { return m_screenSize; }
    int l_GetScreenSize( luabridge::lua_State* L );
    float GetScreenResToWindowSizeRatio() const { return m_screenResToWindowSizeRatio; }

    void HandleTouch( Vector2i& screenTouchPos );

    luabridge::lua_State* GetLuaState() { return m_L; }
    void l_Print( const std::string& s ) { NOVA_LOG( s ); }

    void ShowCursor( bool show ) { SDL_ShowCursor( show ); }

    Application( Application const& ) = delete;
    void operator=( Application const& ) = delete;

private:
    Application()
        : m_window( NULL )
        , m_renderer( NULL )
        , m_root( nullptr )
        , m_overlay( nullptr )
        , m_screenResToWindowSizeRatio( 1 )
    {}

    void RenderFrame();
    void Update( uint32_t dt );
    void AddLuaWrappers();

    SDL_Window* m_window;
    SDL_Renderer* m_renderer;
    std::shared_ptr<Node> m_root;
    std::shared_ptr<Node> m_overlay;

    float m_screenResToWindowSizeRatio;

    CallbacksMap<Callback<uint32_t>, uint32_t> m_updateCallbacks;
    CallbacksMap<CallbackLua<uint32_t>, uint32_t> m_luaUpdateCallbacks;

    Vector2i m_screenSize;

    luabridge::lua_State* m_L;
};

#endif // !__APPLICATION_HPP__
