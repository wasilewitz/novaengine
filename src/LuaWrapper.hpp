#ifndef __LUA_WRAPPER_HPP__
#define __LUA_WRAPPER_HPP__
#include <LuaBridge.h>
#include "Application.hpp"

#define Lua LuaWrapper::GetInstance()

class LuaWrapper
{
public:
    static LuaWrapper& GetInstance()
    {
        static LuaWrapper instance;
        return instance;
    }

    void DoFile( const std::string& fileName );

private:
    LuaWrapper()
    {}
};

#endif
