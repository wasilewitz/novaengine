#ifndef __RECT_HPP__
#define __RECT_HPP__

#include "Math\Vector2.hpp"

class Rect
{
public:
    Rect( uint32_t x, uint32_t y, uint32_t width, uint32_t height )
        : m_x( x )
        , m_y( y )
        , m_width( width )
        , m_height( height )
    {
    }

    Rect()
        : m_x( 0 )
        , m_y( 0 )
        , m_width( 0 )
        , m_height( 0 )
    {
    }

    Math::Vector2i GetPosition() const { return Math::Vector2i( m_x, m_y ); }
    Math::Vector2i GetSize() const { return Math::Vector2i( m_width, m_height ); }
private:
    uint32_t m_x;
    uint32_t m_y;
    uint32_t m_width;
    uint32_t m_height;
};

inline bool operator==( Rect& rect1, Rect& rect2 )
{
    return rect1.GetPosition() == rect2.GetPosition() && rect1.GetSize() == rect2.GetSize();
}

inline bool operator!=( Rect& rect1, Rect& rect2 )
{
    return !( rect1 == rect2 );
}

inline bool operator==( const Rect& rect1, const Rect& rect2 )
{
    return rect1.GetPosition() == rect2.GetPosition() && rect1.GetSize() == rect2.GetSize();
}

inline bool operator!=( const Rect& rect1, const Rect& rect2 )
{
    return !( rect1 == rect2 );
}

#endif // !__RECT_HPP__
