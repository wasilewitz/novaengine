#ifndef __TEXTURE_RESOURCE_HPP__
#define __TEXTURE_RESOURCE_HPP__

#include <SDL.h>
#include <SDL_image.h>
#include "Utilities.hpp"
#include <memory>

class TextureData;

class TextureResource
{
public:
    TextureResource( const std::string& path );
    ~TextureResource();

    TextureData* GetTextureData() const { return m_textureData.get(); }
    const uint16_t GetReferenceCount() const { return m_referenceCount; }
    const std::string& GetPath() const { return m_path; }

    void AddReference();
    void RemoveReference();

private:
    std::unique_ptr<TextureData> m_textureData;
    SDL_Texture* m_texture;
    uint16_t m_referenceCount;
    std::string m_path;
};

class TextureData
{
public:
    TextureData( SDL_Texture* texture )
        : m_texture( texture )
    {
        SDL_QueryTexture( m_texture, NULL, NULL, &m_width, &m_height );
    }

    int GetWidth() { return m_width; }
    int GetHeight() { return m_height; }
    SDL_Texture* GetTexture() { return m_texture; }

private:
    SDL_Texture* m_texture;
    int m_width;
    int m_height;
};

#endif // !__TEXTURE_RESOURCE_HPP__
